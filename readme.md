# PHP haladó tanfolyam #

##1.
Szintfelmérő - kereső program írása - fejlesztőkörnyezet kialakítása (xampp,netbeans,git-scm)

##2.
GIT alapok - git-bash,git,config (user.name,user.email,ssh-keygen,init,add,commit,status,log,remote,push)

-OOP mappa és keret kialakítása az objektum orientált php megismeréséhez (bootstrap 3)

##3. 
Osztály feladata, definiálása, példaosztály létrehozása (Address)

-Obj tulajdonságok meghatározása (public,protected)

-Obj metódus létrehozása (public display)

-Magic metódusok bemutatása, létrehozása (
```
#!php

__construct, __get, __set
```
)

##4. 
Fejlesztési esettanulmány, specifikáció fontossága, elemei, példa specifikáció megírása a címek kezeléséhez

-Munka a verziókezelő rendszeren 2. (merge, pull, conflict kezelés)

-Statikus osztály tulajdonság és osztály állandó, :: scope reolution operator, 
```
#!php

__toString
```

