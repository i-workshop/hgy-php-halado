<?php

/**
 * Saját exception handler
 */
class ExceptionAddress extends Exception {
    public function __toString() {
        return __CLASS__ . ': ['.$this->code.'] : '.$this->message.'<br>';
    }
}