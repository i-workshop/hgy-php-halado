<?php

/* 
 * TODO make the connection
 */

class Database{
    //kapcsolat tárolása
    private $_connection;
    //példány tárolása
    private static $_instance;
    
    /**
     * Gondoskodjunk pontosan egy példány létezéséről a Database osztályból
     * return Database
     */
    public static function getInstance(){
        if(!self::$_instance){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    /**
     * konstruktor
     */
    public function __construct() {
       $this->_connection = new mysqli('localhost', 'root', '', 'hgy_php_halado');
       
       //esetleg keletkező hiba kezelése
       if(mysqli_connect_error()){
           trigger_error('Nem sikerült a db csatlakozás: '.mysqli_connect_error(), E_USER_ERROR);
       }
    }
 //védekezés az obj klónozás ellen
    private function __clone(){}
    
    /**
     * private connection visszaadása kérésre
     */
    public function getConnection(){
        return $this->_connection;
    }
}