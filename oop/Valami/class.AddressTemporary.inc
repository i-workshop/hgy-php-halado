<?php
/**
 * Ideiglenes cím bővítés
 */
class AddressTemporary extends Address {
     //metódus felülírás
    public function display(){
        $output='<div class="col-xs-12 business alert alert-warning">';
        $output.= parent::display();//ősből ami kiírja a cím többi részét
        $output.='</div>';
        return $output;
    }
    /**
     * beállítás (inicializálás)
     */
    protected function _init(){
        $this->_setAddressTypeId(parent::ADDRESS_TYPE_TEMPORARY);
    }
}