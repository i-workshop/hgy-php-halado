<?php
namespace Valami;
/**
 * Címeket kezelő osztály - nem példányosítható ős
 */
abstract class Address implements Model {
   //osztály állandók (constans)
   const ADDRESS_TYPE_RESIDENCE = 1;
   const ADDRESS_TYPE_BUSINESS = 2;
   const ADDRESS_TYPE_TEMPORARY = 3;
   //hibakódok az osztályból
   const ADDRESS_ERROR_NOT_FOUND =1000;
   const ADDRESS_ERROR_UNKNOWN_SUBCLASS=1001;
   //tárolt címek lehetséges tipusai
   public static $valid_address_types=[
      Address::ADDRESS_TYPE_RESIDENCE => 'Residence',
      self::ADDRESS_TYPE_BUSINESS => 'Business',
      Address::ADDRESS_TYPE_TEMPORARY => 'Temporary',      
   ] ;
   //Címsorok
   public $street_address_1; 
   public $street_address_2;
   //Város neve
   public $city_name;
   //régió neve
   public $subdivision_name;
   
   //ország neve
   public $country_name;
   //irányítószám
   protected $_postal_code;
   //cím azonosító (primary key)
   protected $_address_id;
   //cím tipus azonosító
   protected $_address_type_id;
   //mikor készült a bejegyzés és mikor módosították
   protected $_time_created;
   protected $_time_updated;
   
   
   public function __construct($data=[]) {
       $this->_init();//bővítésekből jön
       $this->_time_created = time();
       //ha cím nem származtatható le az adatokból
       if(!is_array($data)){
           trigger_error('Nem lehet objektumot származtatni a kapott adatokból a '.get_class($name).' osztállyal');
       }elseif(count($data)>0){
       
        //a kapott adatokból építsük fel a cím objektumot
           foreach($data as $name => $value){
               //kivételezés a time created és time updated tul okra, mivel ezek jöhetnek tárolt adatokból is (db)
               if(in_array($name,['time_created','time_updated','address_id','address_type_id']                          )){
                       $name='_'.$name;
               }
               $this->$name = $value;
           }
       }
      
   }
   /**
    * Magic __get
    * @param string $name
    * @return mixed
    */
   public function __get($name) {
       //ha nincs védett postal_codeunk akkor vszinu keresni kell a db ben
       //var_dump($name);
    if(!$this->_postal_code){
        $this->_postal_code = $this->_postal_code_search();
    } 
    
    //kísérlet viszzatérésre egy védett tulajdonságnévvel
    $protected_property_name = '_'.$name;
    if(property_exists($this,$protected_property_name)){
        return $this->$protected_property_name;
    }
    
    //nem sikerült a tul elérése
    trigger_error('Nem meghatározott tulajdonság __get által:'.$name);
    return NULL;
   }
   /**
    * 
    * @param string $name
    * @param type mixed
    * @return mixed
    */
   public function __set($name, $value) {
    
       //irsz szám beállítása bárhonnan
       if($name=='postal_code'){
           $this->$name=$value;
           return;
       }
       //Nem lehet elérni a tulajdonságot, trigger error
       trigger_error('Nem lehet elérni a tulajdonságot __set-tel:'.$name);
   }
   /**
    * Objektum írja ki magát Magic __toString
    * @return string
    */
   public function __toString() {
       return $this->display();
   }
   
   public function __clone() {
       $this->_time_created = time();
       $this->_time_updated = time();
   }
   /**
    * Kényszerítsük a bővítéseket hogy tartalmazniuk kelljen egy ilyen metódust
    */
   abstract protected function _init();
   /**
    * Irányítószám keresése város és régiónév alapján
    * @todo Lecserélni a valódi db lekérésre
    * @return string
    */
   protected function _postal_code_search(){
       //kérjünk egy Database példányt
       $db= Database::getInstance();
       $mysqli= $db->getConnection();
       $mysqli->set_charset('utf8');//kódlap illesztés
       //lekérés összeállítása
       $qry="SELECT irsz ";
       $qry.=" FROM telepulesek ";
       $qry.=" WHERE ";
       //város név
       $city_name=$mysqli->real_escape_string($this->city_name);
       $qry.= " telepules_nev='$city_name' ";
       //település rész neve az irsz-ok megkülönböztetéséhez
       $subdivision_name=$mysqli->real_escape_string($this->subdivision_name);
       $qry.= " AND telepules_resz='$subdivision_name' ";
       
       $result= $mysqli->query($qry);//lekérés
    
       if( $row=$result->fetch_assoc() ){//kibontás és ha van visszatérünk a talált elemmel
           return $row['irsz'];
       }       
   }
   
   /**
    * Cím HTML kiírása bootstrapbe 
    * @return string
    * 1234, Kukutyin, Valami utca. 14.
    * V. emelet 8/a
    * Vas megye
    * Magyarország
    */
   public function display(){
       $output='';
       //irsz
       $output .= '<span>'.$this->postal_code.',</span>';
       //város
       $output .= '<span>'.$this->city_name.',</span>';
       //címsor 1
       $output .= '<span>'.$this->street_address_1.'</span>';
       //ha van címsor 2
       if($this->street_address_2){
           $output .= '<div>'.$this->street_address_2.'</div>';
       }
       //régió ha meg van adva
       if($this->subdivision_name){
           $output .= '<div>'.$this->subdivision_name.'</div>';
       }
       //ország
       $output .= '<div>'.$this->country_name.'</div>';
       //térjünk vissza a kapott stringgel
       return $output;
   }
   /**
    * Mondjuk meg egy kapott értékről, hogy létező címtipus azonosító-e
    * @param int $address_type_id
    * @return boolean
    */
   static public function isValidAddressTypeId($address_type_id){
       return array_key_exists($address_type_id,self::$valid_address_types);
   }
   /**
    * Ha érvényes a kapott címtipus, állitsuk be
    * @param int $address_type_id
    */
   protected function _setAddressTypeId($address_type_id){
       if(self::isValidAddressTypeId($address_type_id)){
           $this->_address_type_id = $address_type_id;
       }
   }
   
   /**
    * Cim betöltése
    * @param int $address_id
    */
   final public static function load($address_id){
       //kérjünk egy Database példányt
       $db= Database::getInstance();
       $mysqli= $db->getConnection();
       $mysqli->set_charset('utf8');//kódlap illesztés
       //lekérés összeállítása
       $qry="SELECT * ";
       $qry.=" FROM addresses ";
       $qry.=" WHERE address_id = ". (int) $address_id;
       $qry.=" LIMIT 1";
       //lekérés
       $result = $mysqli->query($qry);
       //ha van adjuk vissza az objektumot a megfelelő classból
       if($row = $result->fetch_assoc()){
           return self::getInstance($row['address_type_id'], $row);
       }
       //cím nem található hiba kezelése
       throw new ExceptionAddress('Nem találtam a címet!',self::ADDRESS_ERROR_NOT_FOUND);
   }
   /**
    * Address_type_id alapján adjunk vissza példányt a megfelelő alosztályból (subclass)
    * @param int $address_type_id
    * @param array $data
    * @return Address subclass
    */
   final public static function getInstance($address_type_id,$data=[]){
       if(self::isValidAddressTypeId($address_type_id)){
       $class_name = 'Address'. self::$valid_address_types[$address_type_id];
        return new $class_name($data);
       }else{
            throw new ExceptionAddress('Address alosztály (subclass) nem található, objektum nem hozható létre - helytelen hívás - ',self::ADDRESS_ERROR_UNKNOWN_SUBCLASS);
       }
       
       //var_dump(get_parent_class($class_name));
       if(!class_exists($class_name)){//hf, igy nem jó, javítani
           throw new ExceptionAddress('Address alosztály (subclass) nem található, objektum nem hozható létre -'.$class_name.' - ',self::ADDRESS_ERROR_UNKNOWN_SUBCLASS);
       }
      
   }
   /**
    * Cím mentése
    */
   final public function save(){
       //TODO save an address object to database
   }
}