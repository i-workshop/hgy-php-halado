<?php
/**
 * Interface az interakciókhoz
 */
interface Model {
    /**
     * model betöltése
     * @param int $address_id
     */
    static function load($address_id);
    
    /**
     * model mentése
     */
    function save();
}