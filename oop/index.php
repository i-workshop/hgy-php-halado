<!DOCTYPE html>
<html lang="hu">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP OOP tanfolyam - Ruander oktatóközpont</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

  </head>
  <body>
 <div class="container">
    <header>
	 <h1>PHP OOP tanfolyam</h1>
	</header>
	<article>
	<?php require 'test.php'; ?>
	</article>
	<footer>
	Ruander oktatóközpont - Horváth György - <?php echo date('Y-m-d H:i:s - l');?>
	</footer>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>