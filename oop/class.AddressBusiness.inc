<?php
/**
 * Számlázási cím bővítés
 */
class AddressBusiness extends Address {
    //konstansokat és tulajdonságokat ujra lehet deklarálni
    
    //metódus felülírás
    public function display(){
        $output='<div class="col-xs-12 business alert alert-success">';
        $output.= parent::display();//ősből ami kiírja a cím többi részét
        $output.='</div>';
        return $output;
    }
    
    /**
     * beállítás (inicializálás)
     */
    protected function _init(){
        $this->_setAddressTypeId(parent::ADDRESS_TYPE_BUSINESS);
    }
}