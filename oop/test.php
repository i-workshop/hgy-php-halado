<?php
/**
 * Autoloader az osztályok betöltésére
 * @param string $class_name
 */
function __autoload($class_name) {
    include 'class.' . $class_name . '.inc';
}

echo '<h2>Cím osztály példányosítása</h2>';
try{
    $address_residence = new AddressResidence;
    $address_residence->street_address_1 = 'Frangepán utca 3.';
    $address_residence->city_name = "Körmend";
    $address_residence->subdivision_name = '';
    $address_residence->country_name = 'Magyarország';
    echo '<pre>' . var_export($address_residence, true) . '</pre>';
    echo $address_residence;
}
catch(ExceptionAddress $e){
    echo $e;
}


